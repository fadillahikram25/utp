package com.company;
import java.util.Scanner;
public class Kasir {
    private Meja[] daftarMeja;
    private Menu[] daftarMenu;

    public Kasir() {
        daftarMeja = new Meja[10];
        for (int i = 0; i < 10; i++) {
            daftarMeja[i] = new Meja(i + 1);
        }

        daftarMenu = new Menu[5];
        daftarMenu[0] = new Menu("Nasi Goreng", 15000);
        daftarMenu[1] = new Menu("Mi Goreng", 15000);
        daftarMenu[2] = new Menu("Capcay", 20000);
        daftarMenu[3] = new Menu("Bihun Goreng", 17000);
        daftarMenu[4] = new Menu("Ayam Koloke", 25000);
    }

    // digunakan untuk menampilkan daftar meja beserta keterangan ketersediaannya
    // gunakan method isKosong pada class Kasir agar lebih mudah
    public void tampilkanDaftarMeja() {
        for (Meja meja : daftarMeja) {
            if (!meja.isKosong()) {
                System.out.println("Meja "+meja.getNomorMeja()+" (terisi oleh pelanggan "+meja.getPelanggan().getNama()+")");
            } else {
                System.out.println("Meja "+meja.getNomorMeja()+" (kosong)");
            }
        }


        // EDIT DISINI
    }

    // untuk menambahkan pelanggan pada meja tertentu
    // jika meja kosong tambahkan pelanggan pada meja tersebut
    // jika tidak buatlah keterangan bahwa meja sudah ada pelanggan
    public void tambahPelanggan(int nomorMeja, Pelanggan pelanggan) {
        Meja meja = daftarMeja[nomorMeja - 1];
        if (meja.getPelanggan() == null) {
            meja.setPelanggan(pelanggan);
        }
        else {
            System.out.println("Meja sudah terisi");
        }
        // EDIT DISINI
    }

    // menambah pesanan menu pada nomor meja
    // jika menu tidak ada dalam daftar maka tampilkan "Menu is null"
    public void tambahPesanan(int nomorMeja, Menu menu) {
        boolean ada = false;
        Meja meja = daftarMeja[nomorMeja-1];
        for (Menu m : daftarMenu) {
            if (m == menu) {
                ada = true;
                break;
            }
        }
        if (ada) {
            meja.setMenu(menu);
        } else {
            System.out.println("Menu is null");
        }
        // EDIT DISINI
    }

    // Menghapus pelanggan
    public void hapusPelanggan(int nomorMeja) {
        Meja meja = daftarMeja[nomorMeja-1];
        if (meja.isKosong()) {
            System.out.println("Meja belum terisi");
        } else {
            meja.setPelanggan(null);
        }
    }

    public int hitungHargaPesanan(int nomorMeja) {
        int totalHarga = 0;
        Meja meja = daftarMeja[nomorMeja - 1];
        Pelanggan pelanggan = meja.getPelanggan();
        Menu[] menu = meja.getMenu();
        if (menu != null && pelanggan != null && menu.length > 0) {
            for (int i = 0; i < menu.length; i++) {
                if (menu[i] != null) {
                    totalHarga = menu[i].getHarga() + totalHarga;
                }
            }
            return totalHarga;
        }
        return totalHarga;
    }

    public void tampilkanPesanan(int nomorMeja) {
        Meja meja = daftarMeja[nomorMeja - 1];
        Pelanggan pelanggan = meja.getPelanggan();
        Menu[] menu = meja.getMenu();
        if (menu != null  && menu.length > 0   && pelanggan != null) {
            for (int x = 0; x < menu.length; x++) {
                if (menu[x] != null) {
                    System.out.println("Meja " + nomorMeja + " - " + pelanggan.getNama() + " memesan "
                            + menu[x].getNama() + " seharga " + menu[x].getHarga());
                }
            }
        } else {
            System.out.println("Meja " + nomorMeja + " tidak memiliki pesanan");
        }
    }

    public void tampilkanDaftarMenu() {
        System.out.println("Daftar Menu:");
        System.out.println("1. Nasi Goreng - Rp15.000");
        System.out.println("2. Mi Goreng - Rp15.000");
        System.out.println("3. Capcay - Rp20.000");
        System.out.println("4. Bihun Goreng - Rp17.000");
        System.out.println("5. Ayam Koloke - Rp25.000");
        System.out.println("6. Simpan");
        System.out.println();
    }

    public void tampilkanDaftarFitur() {
        System.out.println("1. Tampilkan daftar meja");
        System.out.println("2. Tambah pelanggan");
        System.out.println("3. Tambah pesanan");
        System.out.println("4. Hapus pelanggan");
        System.out.println("5. Hitung harga pesanan");
        System.out.println("6. Tampilkan pesanan di meja");
        System.out.println("0. Keluar");
    }

    public void jalankan() {
        Scanner scanner = new Scanner(System.in);
        int pilihan = -1;
        while (pilihan != 0) {
            tampilkanDaftarFitur();
            System.out.print("Masukkan pilihan: ");
            pilihan = scanner.nextInt();
            scanner.nextLine();
            switch (pilihan) {
                case 1:
                    tampilkanDaftarMeja();
                    break;
                    // menampilkan daftar meja dengan method yang sudah ada
                case 2:
                    System.out.print("No meja: ");
                    int nomorMeja = scanner.nextInt();
                    scanner.nextLine();
                    System.out.print("Nama pelanggan: ");
                    String nama = scanner.nextLine();

                    Pelanggan x = new Pelanggan(nama);
                    tambahPelanggan(nomorMeja, x);
                    break;
                    // tampilkan pesan untuk input nomor meja dan nama pelanggan untuk digunakan
                    // pada method
                    // instansiasi Pelanggan dengan nama pelanggan sesuai input
                case 3:
                    boolean stopLoop = false;
                    System.out.print("Masukkan nomor meja: ");
                    int nomorPesanan = scanner.nextInt();
                    Boolean meja = daftarMeja[nomorPesanan - 1].isKosong();
                    scanner.nextLine();
                    if (!meja) {
                        tampilkanDaftarMenu();
                        while (!stopLoop) {
                            System.out.print("Masukkan nomor menu: ");
                            int nomorMenuPesan = scanner.nextInt();
                            scanner.nextLine();
                            switch (nomorMenuPesan) {
                                case 1:
                                    tambahPesanan(nomorPesanan, daftarMenu[0]);
                                    break;
                                case 2:
                                    tambahPesanan(nomorPesanan, daftarMenu[1]);
                                    break;
                                case 3:
                                    tambahPesanan(nomorPesanan, daftarMenu[2]);
                                    break;
                                case 4:
                                    tambahPesanan(nomorPesanan, daftarMenu[3]);
                                    break;
                                case 5:
                                    tambahPesanan(nomorPesanan, daftarMenu[4]);
                                    break;
                                case 6:
                                    stopLoop = true;
                                    break;
                                default:
                                    System.out.println("Nomor tidak tersedia");
                                    break;
                            }
                        }
                    } else {
                        System.out.println("Meja belum terisi oleh pelanggan");
                    }
                    break;
                case 4:
                    System.out.print("Nomor meja: ");
                    int NomorMeja = scanner.nextInt();
                    hapusPelanggan(NomorMeja);
                    break;
                    // untuk menghapus pelanggan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja yang akan dihapus untuk digunakan
                    // pada method hapusPelanggan()
                case 5:
                    System.out.print("no meja: ");
                    int Nomeja = scanner.nextInt();
                    int total = hitungHargaPesanan(Nomeja);
                    if (total == 0) {
                        System.out.println("Meja tidak memilikki pesanan");
                    } else {
                        System.out.println("harga pesanan pada meja no "+Nomeja+" adalah "+total);
                    }
                    break;

                    // Untuk melihat total harga pesanan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja
                    // jangan lupa membedakan keluaran apabila pelanggan belum memesan apapun /
                    // total harga 0
                case 6:
                    System.out.print("Nomor meja: ");
                    int Nomermeja = scanner.nextInt();
                    tampilkanPesanan(Nomermeja);
                    break;
                    // untuk melihat pesanan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja

                case 0:
                    System.out.println("Terima kasih telah menggunakan aplikasi kasir restoran!");
                    break;
                default:
                    System.out.println("Pilihan tidak valid");
                    break;
            }
            System.out.println();
        }
        scanner.close();
    }


}
